#!/bin/bash
docker-compose up -d

# Construction des images
docker build -t mynginx http/
docker build -t myphp script/
docker build -t mymariadb data/

# Création du réseau Docker
docker network create mynetwork

# Démarrage des containers
docker run -d --name http --network mynetwork -p 8080:8080 mynginx
docker run -d --name script --network mynetwork myphp
docker run -d --name data --network mynetwork mymariadb
