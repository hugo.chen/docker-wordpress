#!/bin/bash

# créer le réseau nécessaire à la mise en communication des 2 containers
docker network create myreseau

# créer et lancer le container php auquel on a fourni le code source des fichiers nécessaires
docker container run -d --network myreseau --name script -v "${PWD}/etape1/source:/var/www/html" php:8.3.7-fpm

# créer et lancer le container nginx qu'on reconfiguré et dans lequel on a ajouté les codes sources
docker container run -p 8080:80 -d --name http -v "${PWD}/etape1/source:/var/www/html" -v "${PWD}/etape1/conf/default.conf:/etc/nginx/conf.d/default.conf" --network myreseau nginx:1.25