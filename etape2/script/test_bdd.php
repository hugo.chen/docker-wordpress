<?php
// Déclaration des variables de connexion à la base de données
$serveur = "data"; 
$username = "testuser"; 
$password = "testpassword"; 
$dbname = "testdb"; 
try {
    // Tentative de connexion à la base de données avec PDO + erreur
    $conn = new PDO("mysql:host=$serveur;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Préparation d'une requête SQL pour insérer une valeur dans la table TestTable
    $stmt = $conn->prepare("INSERT INTO TestTable (value) VALUES (:value)");
    // Liaison du paramètre 'value' à la variable $value
    $stmt->bindParam(':value', $value);
    // Attribution d'une valeur aléatoire à la variable $value
    $value = rand();
    // Exécution 
    $stmt->execute();

    // Préparation d'une requête SQL pour sélectionner les colonnes 'id' et 'value' de la table TestTable
    $stmt = $conn->prepare("SELECT id, value FROM TestTable");
    // Exécution de la requête 
    $stmt->execute();
    // Récupération de tous les résultats de la requête sous forme de tableau
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Conversion du tableau en format JSON et affichage du résultat
    echo json_encode($result);
} catch (PDOException $e) {
    // En cas d'exception affichage du message d'erreur
    echo "Connection échouée: " . $e->getMessage();
}
?>
